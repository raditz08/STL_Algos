
#include<iostream>
#include<vector>
#include<deque>
#include<iterator>
#include<algorithm>
#include<fstream>
#include<list>
#include<string>
#include<cctype>
#include<numeric>
using namespace std;


int ciCharCompare(const char& c1, const char& c2)
{
	int l1 = tolower(static_cast<unsigned char>(c1));
	int l2 = tolower(static_cast<unsigned char>(c2));

	if(l1<l2) return -1;
	if(l1>l2) return 1;

	return 0;
}


int ciStringCompareImpl(const string& s1, const string& s2)
{
	typedef pair<string::const_iterator, string::const_iterator> PSCI;
	
	PSCI p = mismatch(
			s1.begin(), s1.end(),
			s2.begin(),
			not2(ptr_fun(ciCharCompare))
			);

	if(p.first==s1.end())
	{
		if(p.second==s2.end()) return 0;
		else return -1;
	}
	return ciCharCompare(*p.first, *p.second);	//Mismatch occurs and we return comparison value
}
int ciStringCompare(const string& s1, const string& s2)
{
	if(s1.size()<=s2.size()) return ciStringCompareImpl(s1,s2);
	else return -ciStringCompareImpl(s1,s2); 
}


//Use of accumulate, for_each
struct Point
{
	double x, y;
	//size_t numPoints;

	Point(double x_copy, double y_copy)
	{x = x_copy; y = y_copy;}
};

class Point_Avg:
	public unary_function<Point, void>{

		public:
			Point_Avg():numPoints(0), xSum(0), ySum(0){}

			void operator()(const Point& p)
			{
				++numPoints;
				xSum+=p.x;
				ySum+=p.y;
			}

			Point result() const
			{
				return Point(xSum/numPoints, ySum/numPoints);
			}
		private:
			size_t numPoints;
			double xSum, ySum;
		};

                                                          
int main(void)
{
	string S1("WOrDtocompare1");
	string S2("wOratOcoMpare2");

	ifstream co("Iter_file.txt");

//	cout << accumulate(istream_iterator<int>(co),
	//			istream_iterator<int>(),
	//			0) << '\n';
//	cout << ciStringCompare(S1,S2) << '\n';



	ifstream Pt_file("Point_data.txt");
	istream_iterator<double> dataBegin(Pt_file);
	istream_iterator<double> dataEnd;

	list<double> data(dataBegin, dataEnd);

	for(list<double>::iterator i1 = data.begin();i1!=data.end();i1++)
		cout << *i1 << '\n';

	Point avg_pt = for_each( data.begin(), data.end(), Point_Avg() ).result();

	//cout << '\n' << avg_pt.numPoints << '\n';

	return 0;
}
